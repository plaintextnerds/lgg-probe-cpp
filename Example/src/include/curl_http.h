//
// Created by plain on 10/12/2021.
//

#ifndef LATENCYGG_CURL_HTTP_H
#define LATENCYGG_CURL_HTTP_H
#include <curl/curl.h>
#include <external/nlohmann/json.hpp>

nlohmann::json get_metrics();

#endif //LATENCYGG_CURL_HTTP_H
