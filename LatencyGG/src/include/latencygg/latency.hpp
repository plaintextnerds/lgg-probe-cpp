//
// Created by tim on 16/08/2021.
//

#include <string>
#include <vector>
#include <unordered_map>
#if defined(_MSC_VER)
#include <WinSock2.h>

#else
#include <arpa/inet.h>
#endif

#include "latencygg/measurements/dataping.hpp"


#ifndef LATENCYGG_LATENCY_H
#define LATENCYGG_LATENCY_H


namespace latencygg {
    using latencygg::dataping::DataPing;

    enum af_t : uint16_t {
        eINET = AF_INET, eINET6 = AF_INET6
    };

    template<class T>
    class LatencyGG {
        std::unordered_map<std::string, T> mPings;
        af_t mAf;
        bool mKilled = false;
        bool mStopped = false;
        unsigned int mTimeout = 2000;
        timestamp_millisec_t mStartTime;
    public:
        LatencyGG();

        LatencyGG(af_t, unsigned int);

        typename std::unordered_map<std::string, T>::iterator begin();

        typename std::unordered_map<std::string, T>::iterator end();

        size_t size();

        std::vector<std::string> getKeys();

        T operator[](const std::string &aKey);

        void add(const DataPing &aPing);

        void run();

        bool isRunnable();

        bool isComplete();

        void stop();

        void kill();
    };

}

#endif // LATENCYGG_LATENCY_H