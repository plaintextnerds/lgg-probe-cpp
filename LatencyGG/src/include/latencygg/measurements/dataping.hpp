//
// Created by tim on 16/08/2021.
//
#include "latencygg/measurements/dataping_wire.hpp"


#ifndef LATENCYGG_DATAPING_H
#define LATENCYGG_DATAPING_H


namespace latencygg {

    struct Stats {
        uint32_t mRtt;
        uint32_t mStddev;
    };

    namespace dataping {
        using namespace latencygg::wire;

        enum data_ping_state_t {
            eKilled = -2, eEmpty = -1, eC0 = 0, eS0, eC1, eS1, eC2, eComplete
        };

        class DataPing {
            data_ping_state_t mState = eC0;
            std::string mVersion = "v0.0.0-local";
            std::string mIdent;
            std::string mToken;
            timestamp_millisec_t mTimestampC0, mTimestampS0, mTimestampC1, mTimestampS1, mTimestampC2, mTimestampS2, mTimestampC3;
            signature_t mSignatures[3]{};
            uint16_t mSeq;
            int mDPVersion = 3;
            int mErrors = 0;
        public:
            std::string mTargetIp;
            std::string mSourceIp;

            DataPing();

            DataPing(std::string aIdent, std::string aSourceIp, std::string aTargetIp, std::string aToken, int aDPVersion);

            void overwrite(DataPing &dataPing);

            void update(std::vector<uint8_t> aReply, size_t aReplyLength);

            std::vector<uint8_t> generatePacket();

            bool isComplete();

            bool isDead();

            bool isEmpty();

            std::shared_ptr<Stats> getStats();

            std::string asJson();

            void kill();
        };

    }
}
#endif // LATENCYGG_DATAPING_H