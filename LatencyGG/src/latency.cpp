//
// Created by tim on 17/08/2021.
//
#if defined(_MSC_VER)
#include "targetver.h"
#endif

#include <chrono>
#include <experimental/networking.hpp>

#include "latencygg/common.hpp"
#include "latencygg/latency.hpp"


#define MAX_MTU 1500


namespace latencygg {
    using std::experimental::net::ip::address;
    using std::experimental::net::ip::make_address;
    using std::experimental::net::buffer;
    using std::experimental::net::io_context;
    using std::experimental::net::ip::udp;
    using namespace std::chrono;


    template<class T>
    LatencyGG<T>::LatencyGG() {
        mAf = eINET;
        mPings = std::unordered_map<std::string, T>();
    }

    template<class T>
    LatencyGG<T>::LatencyGG(af_t aAf, unsigned int aTimeout) : LatencyGG<T>() {
        mAf = aAf;
        mTimeout = aTimeout;
        mPings = std::unordered_map<std::string, T>();
    }

    template<class T>
    void LatencyGG<T>::add(const DataPing &aPing) {
        mPings[aPing.mTargetIp] = aPing;
    }

    template<class T>
    typename std::unordered_map<std::string, T>::iterator LatencyGG<T>::begin() {
        return mPings.begin();
    }

    template<class T>
    typename std::unordered_map<std::string, T>::iterator LatencyGG<T>::end() {
        return mPings.end();
    }

    template<class T>
    std::vector<std::string> LatencyGG<T>::getKeys() {
        auto key_selector = [](auto pair){return pair.first;};
        std::vector<std::string> keys(mPings.size());
        for (auto&[target, _]: mPings) {
            transform(mPings.begin(), mPings.end(), keys.begin(), key_selector);
        }
        return keys;
    }

    template<class T>
    size_t LatencyGG<T>::size() {
        return mPings.size();
    }

    template<class T>
    T LatencyGG<T>::operator[](const std::string &aKey) {
        return mPings[aKey];
    }

    template<class T>
    void LatencyGG<T>::run() {
        mStartTime = now();
        io_context io_context;
        std::shared_ptr<udp::socket> s;
        udp::endpoint target_endpoint;
        udp::endpoint sender_endpoint;
        udp::resolver resolver(io_context);
        unsigned short port;
        std::vector<uint8_t> reply;
        reply.resize(MAX_MTU);
        switch (this->mAf) {
            case eINET6:
                s = std::make_shared<udp::socket>(io_context, udp::endpoint(udp::v6(), 0));
                port = 9999;
                break;
            default:
            case eINET:
                s = std::make_shared<udp::socket>(io_context, udp::endpoint(udp::v4(), 0));
                port = 9998;
                break;
        }
        if (this->isRunnable()) {
            for (auto&[target, ping]: mPings) {
                target_endpoint = udp::endpoint(make_address(target), port);
                if (!ping.isComplete() && !ping.isEmpty() && !ping.isDead()) {
                    auto out_packet_buff = ping.generatePacket();
                    s->send_to(buffer(out_packet_buff, out_packet_buff.size()), target_endpoint);
                }
            }
        }
        while (this->isRunnable()) {
            if (s->available() < 112) {
                std::this_thread::sleep_for(100ns);
                continue;
            }
            size_t reply_length = s->receive_from(
                    buffer(reply.data(), MAX_MTU), sender_endpoint
            );
            DataPing &ping = mPings[sender_endpoint.address().to_string()];
            ping.update(reply, reply_length);
            if (!ping.isComplete() && !ping.isDead()) {
                auto out_packet_buff = ping.generatePacket();
                s->send_to(buffer(out_packet_buff, out_packet_buff.size()), sender_endpoint);
            }
        }
        std::vector<std::string> dead_targets;
        for (auto&[target, ping]: mPings) {
            if (!ping.isComplete()) {
                dead_targets.push_back(target);
            }
        }
        for (auto target : dead_targets) {
            mPings.erase(target);
        }
    }

    template<class T>
    bool LatencyGG<T>::isRunnable() {
        bool is_runnable = true;
        auto current_time = now();
        if ((current_time - mStartTime) > mTimeout) {
            is_runnable = false;
        }
        else if (mKilled || mStopped) {
            is_runnable = false;
        }
        else {
            for (auto&[_, ping]: mPings) {
                if (!ping.isComplete() && !ping.isEmpty() && !ping.isDead()) {
                    is_runnable = true;
                    break;
                }
            }
        }
        return is_runnable;
    }

    template<class T>
    bool LatencyGG<T>::isComplete() {
        for (auto&[_, ping]: mPings) {
            if (!ping.isComplete()) {
                return false;
            }
        }
        return true;
    }

    template<class T>
    void LatencyGG<T>::stop() {
        mStopped = true;
    }

    template<class T>
    void LatencyGG<T>::kill() {
        mKilled = true;
        for (auto&[_, ping]: mPings) {
            if (!ping.isComplete()) {
                ping.kill();
            }
        }
    }

    template class LatencyGG<DataPing>;
}
